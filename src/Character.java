import java.awt.*;
import java.util.Optional;

public abstract class Character {
    Optional<Color> display;
    Cell location;

    public Character(Cell location){
        this.location = location;
    }


    public void paint(Graphics g){
        g.setColor(display.orElse(Color.PINK));
        g.fillOval(location.x + location.width/4,
                location.y + location.height/4,
                location.width/2,
                location.height/2);
    }
}
